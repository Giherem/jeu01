package com.example.squelettejeu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;

import java.util.HashSet;
import java.util.Set;

public class LoadAndDrawThread extends Thread {

	public static interface DrawThreadListener{
		public void loadThreadFinished();
	}

	private int canvasHeight = 200;
	private int canvasWidth = 400;
	float offsetX=0;
	float offsetY=0;
	float zoom = 1;

	private final Paint mainPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	LoadModel modele;
	private SurfaceHolder sh;
	Context ctx;

	private boolean run=false;

	Set<DrawThreadListener> listeners = new HashSet<>();
	void addListener(DrawThreadListener l) {listeners.add(l);}
	void removeListener(DrawThreadListener l) {listeners.remove(l);}

	public LoadAndDrawThread(SurfaceHolder surfaceHolder, Context context,
                             LoadModel _modele) {
		sh = surfaceHolder;
		ctx = context;
		modele = _modele;
		mainPaint.setColor(Color.YELLOW);
		mainPaint.setStrokeWidth((float)canvasHeight/50f);
	}

	public void run() {

		// 1- Executer un step du modele
		// 2- Dessiner le modele 
		//(tant qu'on n'a pas fini')

		boolean aFini=false;
		boolean wasTriggered;

		while (run) {
			wasTriggered=modele.isTriggered();
			aFini = !modele.doStep();
			if (aFini) {
				run = false;
			}
			Canvas c = null;
			try {
				c = sh.lockCanvas(null);
				synchronized (sh) {
					drawModele(c,wasTriggered);
				}
			} finally {
				if (c != null) {
					sh.unlockCanvasAndPost(c);
				}
			}
		}

		for (DrawThreadListener l:
			 listeners) {
			if (l == null) continue;
			l.loadThreadFinished();
		}
	}
	
	public void setRunning(boolean b) {
	    run = b;
	}

	public void setSurfaceSize(int width, int height) {
		synchronized (sh) {
			canvasWidth = width;
			canvasHeight = height;
			offsetY = height;
			readaptZoom();
		}
	}
	public void readaptZoom() {
		synchronized (sh) {
			zoom = (float)canvasWidth / 100;
		}
	}

	void drawModele(Canvas canvas,boolean wasTriggered) {
		canvas.drawColor(wasTriggered?Color.WHITE:Color.BLACK);
		canvas.drawLine(0f,canvasHeight/2f,(float)modele.getRemainingSteps()/(float)modele.getInitialSteps()*canvasWidth,canvasHeight/2f,mainPaint);
	}
}
