package com.example.squelettejeu;

public class AimingModel {

    private int mPrecision=-100;
    private int mDispersion=-100;
    private int etat = 2; // 2 : modif precision; 1 : modif dispersion, 0 : OK.

    public AimingModel(){}

    public int getPrecision(){return mPrecision;}
    public int getDispersion(){return mDispersion;}
    public int getEtat(){return etat;}

    public void trigger(){
        if (etat > 0) etat--;
    }

    public boolean doStep() {
        switch (etat) {
            case 2 :
                mPrecision++;
                if (mPrecision >= 100) etat --;
                break;
            case 1 :
                mDispersion++;
                if (mDispersion >= 100) etat --;

            default :
        }
        return (etat != 0);
    }

}
