package com.example.squelettejeu;

import android.net.IpPrefix;

import java.util.Random;

import static java.lang.Math.exp;
import static java.lang.Math.log;

public class GameModel {

	private static final int TAILLE_X = 1000;
	private static final int TAILLE_Y = 2000;

	float mPrecisionGenerale;
	float mDispersionGenerale;

	int[] mParticleAge;
	float[] mParticleDispersiona;
	float[] mParticleDispersionb;
	boolean[] mParticleFlying;
	private int mNbParticles;

	private int mNbHits;
	private int mNbMiss;

	Random rand = new Random();

	public GameModel() {
	}
	
	// Execute un pas de simulation et indique en valeur de retour si on a fini ou pas.
	public synchronized boolean doStep() {
		boolean particleEmmited = false;
		boolean pasFini = false;
		for (int i = 0; i< mNbParticles; i++)
		{
			if (!mParticleFlying[i]) continue;

			pasFini=true;
			if (!particleEmmited && mParticleAge[i]==-1)
			{
				mParticleAge[i]=0;
				particleEmmited=true;
			}
			else
			{
				if (mParticleAge[i]>=0) {
					mParticleAge[i] += 1;
					if (getY(i) < 0.f || mParticleAge[i]>999)
					{
						mParticleFlying[i]=false;
						if (mParticleAge[i] < 910 && mParticleAge[i] > 890)
							mNbHits++;
						else
							mNbMiss++;
					}
				}
			}
		}

		return pasFini;
	};

	public int getX(int index) {return mParticleAge[index];}
	public int getY(int index) {
		float x = mParticleAge[index];
		float y = ((float)exp(mPrecisionGenerale)*-0.00222f* (float)exp(mParticleDispersiona[index]))*x*x + 2.f* (float)exp(mParticleDispersionb[index])*x;
		return (int)y;
	}
	public int getNbParticles() { return mNbParticles;}
	public int getXMax() {return TAILLE_X;}
	public int getYMax() {return TAILLE_Y;}


	// Réglage initial du modèle
	void init(float pPrecision, float pDispersion, int pNbParticules)
	{
		mNbParticles = pNbParticules;
		mPrecisionGenerale = pPrecision;
		mDispersionGenerale = pDispersion;
		mNbHits=0;
		mNbMiss=0;

		mParticleAge = new int[mNbParticles];
		mParticleDispersiona = new float[mNbParticles];
		mParticleDispersionb = new float[mNbParticles];
		mParticleFlying = new boolean[mNbParticles];

		for (int i = 0; i< mNbParticles; i++)
		{
			mParticleAge[i]=-1;
			mParticleDispersiona[i] = mDispersionGenerale*(rand.nextFloat()*2.f -1.f);
			mParticleDispersionb[i] = mDispersionGenerale*(rand.nextFloat()*2.f -1.f);
			mParticleFlying[i]=true;
		}
	}

	int getNbHits() {return mNbHits;}
	int getNbMiss() {return mNbMiss;}

}
