package com.example.squelettejeu;

import java.util.Random;

import static java.lang.Math.exp;

public class GameModel2 {

	private static final int TAILLE_X = 1000;
	private static final int TAILLE_Y = 2000;

	float[] posX;
	float[] posY;

	float[] vitX;
	float[] vitY;
	boolean[] mParticleFlying;
	int[] mParticleAge;
	private int mNbParticles;

	private float mGravity = 22.2f;
	private float mGranularity=1.f/60.f;

	private int mNbHits;
	private int mNbMiss;

	Random rand = new Random();

	public GameModel2() {
	}
	
	// Execute un pas de simulation et indique en valeur de retour si on a fini ou pas.
	public synchronized boolean doStep() {
		boolean particleEmmited = false;
		boolean pasFini = false;
		for (int i = 0; i< mNbParticles; i++) {
			if (!mParticleFlying[i]) continue;
			pasFini = true;
			if (!particleEmmited && mParticleAge[i] == -1) {
				mParticleAge[i] = 0;
				particleEmmited = true;
			}
			else {
				if (mParticleAge[i] < 0) continue;
				vitY[i] -= mGravity*mGranularity;
				posX[i] += vitX[i]*mGranularity;
				posY[i] += vitY[i]*mGranularity;
			}

			if (posY[i] < 0.f) { posY[i]= 0.f; mParticleFlying[i]=false;}
			if (posX[i] > TAILLE_X) { posX[i]= TAILLE_X-1; mParticleFlying[i]=false;}
			mParticleAge[i] += 1;
			if (!mParticleFlying[i])
			{
				if (posX[i] < 910 && posX[i] > 890)
					mNbHits++;
				else
					mNbMiss++;
			}
		}

		return pasFini;
	};

	public int getX(int index) {return (int)posX[index];}
	public int getY(int index) {return (int)posY[index];}
	public int getNbParticles() { return mNbParticles;}
	public int getXMax() {return TAILLE_X;}
	public int getYMax() {return TAILLE_Y;}


	// Réglage initial du modèle
	void init(float pPrecision, float pDispersion, int pNbParticules)
	{
		mNbParticles = pNbParticules;
		posX = new float[pNbParticules];
		posY = new float[pNbParticules];
		vitX = new float[pNbParticules];
		vitY = new float[pNbParticules];
		mParticleFlying = new boolean[pNbParticules];
		mParticleAge = new int[pNbParticules];

		for (int i=0;i<mNbParticles;i++)
		{
			posX[i]=0.f;
			posY[i]=0.f;
			vitX[i]= 100.f + pPrecision + (rand.nextFloat()-0.5f)*pDispersion;
    		vitY[i]= 100.f + pPrecision + (rand.nextFloat()-0.5f)*pDispersion;
    		mParticleFlying[i]=true;
    		mParticleAge[i]=-1;
		}
	}

	int getNbHits() {return mNbHits;}
	int getNbMiss() {return mNbMiss;}

}
