package com.example.squelettejeu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;

import java.util.HashSet;
import java.util.Set;

public class GameAndDrawThread extends Thread {

	public static interface DrawThreadListener{
		public void gameThreadFinished();
	}

	private int canvasHeight = 200;
	private int canvasWidth = 400;
	float offsetX=0;
	float offsetY=0;
	float zoom = 1;

	private final Paint mainPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private final Paint aimPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	GameModel2 modele;
	private SurfaceHolder sh;
	Context ctx;

	private boolean run=false;

	Set<DrawThreadListener> listeners = new HashSet<>();
	void addListener(DrawThreadListener l) {listeners.add(l);}
	void removeListener(DrawThreadListener l) {listeners.remove(l);}

	public GameAndDrawThread(SurfaceHolder surfaceHolder, Context context,
							GameModel2 _modele) {
		sh = surfaceHolder;
		ctx = context;
		modele = _modele;
		mainPaint.setColor(Color.WHITE);
		aimPaint.setColor(Color.RED);
	}




	public void run() {

		// 1- Executer un step du modele
		// 2- Dessiner le modele 
		//(tant qu'on n'a pas fini')

		boolean aFini=false;

		while (run) {
			aFini = !modele.doStep();
			if (aFini) {
				run = false;
			}
			Canvas c = null;
			try {
				c = sh.lockCanvas(null);
				synchronized (sh) {
					drawModele(c);
				}
			} finally {
				if (c != null) {
					sh.unlockCanvasAndPost(c);
				}
			}
		}

		for (DrawThreadListener l:
			 listeners) {
			if (l == null) continue;
			l.gameThreadFinished();
		}
	}
	
	public void setRunning(boolean b) {
	    run = b;
	}

	public void setSurfaceSize(int width, int height) {
		synchronized (sh) {
			canvasWidth = width;
			canvasHeight = height;
			offsetY = height;
			readaptZoom();
		}
	}
	public void readaptZoom() {
		synchronized (sh) {
			zoom = (float)canvasWidth / (float)modele.getXMax();
		}
	}

	void drawModele(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		canvas.drawLine(zoom*890,canvasHeight-1,zoom*910,canvasHeight-1,aimPaint);
		for (int i = 0; i<modele.getNbParticles(); i++)
		{
			canvas.drawCircle(zoom*modele.getX(i),canvasHeight-1-(zoom*modele.getY(i)),2,mainPaint);
//			canvas.drawPoint(zoom*modele.getX(i),zoom*modele.getY(i),mainPaint);
		}
	}
}
