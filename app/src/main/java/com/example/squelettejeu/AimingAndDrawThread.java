package com.example.squelettejeu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;

import java.util.HashSet;
import java.util.Set;

public class AimingAndDrawThread extends Thread {

	public static interface DrawThreadListener{
		public void aimingThreadFinished();
	}

	private int canvasHeight = 200;
	private int canvasWidth = 400;
	float offsetX=0;
	float offsetY=0;
	float zoom = 1;

	Bitmap mCiblefixe;
	Bitmap mCibleMobile;
	Bitmap mCibleDispersion;

	int mBitmapWidth =0;



	private final Paint mainPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	AimingModel modele;
	private SurfaceHolder sh;
	Context ctx;

	private boolean run=false;

	Set<DrawThreadListener> listeners = new HashSet<>();
	void addListener(DrawThreadListener l) {listeners.add(l);}
	void removeListener(DrawThreadListener l) {listeners.remove(l);}

	public AimingAndDrawThread(SurfaceHolder surfaceHolder, Context context,
                               AimingModel _modele) {
		sh = surfaceHolder;
		ctx = context;
		modele = _modele;
		mainPaint.setColor(Color.WHITE);
		mCiblefixe = BitmapFactory.decodeResource(context.getResources(),R.drawable.ciblefixe);
		mCibleMobile = BitmapFactory.decodeResource(context.getResources(),R.drawable.ciblemobile);
		mCibleDispersion = BitmapFactory.decodeResource(context.getResources(),R.drawable.cibledispersion);
		mBitmapWidth = mCiblefixe.getWidth();
	}

	public void run() {

		// 1- Executer un step du modele
		// 2- Dessiner le modele 
		//(tant qu'on n'a pas fini')

		boolean aFini=false;

		while (run) {
			aFini = !modele.doStep();
			if (aFini) {
				run = false;
			}
			Canvas c = null;
			try {
				c = sh.lockCanvas(null);
				synchronized (sh) {
					drawModele(c);
				}
			} finally {
				if (c != null) {
					sh.unlockCanvasAndPost(c);
				}
			}
		}

		for (DrawThreadListener l:
			 listeners) {
			if (l == null) continue;
			l.aimingThreadFinished();
		}
	}
	
	public void setRunning(boolean b) {
	    run = b;
	}

	public void setSurfaceSize(int width, int height) {
		synchronized (sh) {
			canvasWidth = width;
			canvasHeight = height;
			offsetY = height;
			readaptZoom();
		}
	}
	public void readaptZoom() {
		synchronized (sh) {
			zoom = (float)canvasWidth / 100;
		}
	}

	void drawModele(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		switch (modele.getEtat())
		{
			case 2:
				canvas.drawBitmap(mCiblefixe,(float)(canvasWidth-mBitmapWidth)/2f,0.f,mainPaint);
				canvas.drawBitmap(mCibleMobile,(float)canvasWidth*(modele.getPrecision()+100)/200.f-(mBitmapWidth/2f),0.f,mainPaint);
				break;
			case 1:
				canvas.drawBitmap(mCibleDispersion,(float)canvasWidth*(100+modele.getDispersion())/200.f-(mBitmapWidth/2f),0.f,mainPaint);
				canvas.drawBitmap(mCibleDispersion,(float)canvasWidth*(100-modele.getDispersion())/200.f-(mBitmapWidth/2f),0.f,mainPaint);
				break;
			default:

		}
	}
}
