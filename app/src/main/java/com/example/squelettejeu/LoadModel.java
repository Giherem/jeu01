package com.example.squelettejeu;

public class LoadModel {
    private int mInitialSteps;
    private int mRemainingSteps;
    private int mNbProjectiles;
    private boolean mTriggered;

    public LoadModel(int nbStepsInitiaux) {
        mInitialSteps = nbStepsInitiaux;
        mRemainingSteps = nbStepsInitiaux;
        mNbProjectiles=0;
        mTriggered=false;
    }

    public boolean isTriggered(){return mTriggered;}
    public int getEtat(){return (mRemainingSteps<=0)?0:1;}
    public int getInitialSteps() {return mInitialSteps;}
    public int getRemainingSteps() {return mRemainingSteps;}
    public int getNbProjectiles() {return mNbProjectiles;}

    public void trigger(){
        mTriggered=true;
    }

    public boolean doStep() {
        if (mRemainingSteps <= 0) return false;

        mRemainingSteps--;
        if (mTriggered)
        {
            mNbProjectiles++;
            mTriggered=false;
        }

        return true;
    }

}
