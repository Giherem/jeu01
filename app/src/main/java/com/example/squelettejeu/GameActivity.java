package com.example.squelettejeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;

import java.util.Random;

public class GameActivity extends AppCompatActivity implements
        GameAndDrawThread.DrawThreadListener,
        AimingAndDrawThread.DrawThreadListener,
        LoadAndDrawThread.DrawThreadListener,
        View.OnClickListener,
        SurfaceHolder.Callback {

    GameView gv;
    SurfaceHolder sh;

    GameModel2 sm;
    GameAndDrawThread gameThread;

    AimingModel am;
    AimingAndDrawThread aimThread;

    LoadModel lm;
    LoadAndDrawThread loadThread;

    Random rand=new Random();

    int mNbProjectilesObtenus=0;

    int mSurfaceHeight=100;
    int mSurfaceWidth=100;

    private static final int PHASE_INITIAL=0;
    private static final int PHASE_LOADING=1;
    private static final int PHASE_AIMING=2;
    private static final int PHASE_LAUNCHING=3;
    private static final int PHASE_END=4;

    int mPhase = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_game);
        gv = new GameView(this);
        setContentView(gv);

        gv.setOnClickListener(this);

        sh = gv.getHolder();
        sh.addCallback(this);

        lm = new LoadModel(600);
        loadThread = new LoadAndDrawThread(sh,this,lm);
        loadThread.addListener(this);
        am = new AimingModel();
        aimThread = new AimingAndDrawThread(sh,this,am);
        aimThread.addListener(this);
        sm = new GameModel2();
        gameThread = new GameAndDrawThread(sh, this, sm);
        gameThread.addListener(this);

    }

    @Override
    public void gameThreadFinished() {
        mPhase = 3;
        gameThread.removeListener(this);
        int nbHits = sm.getNbHits();
        int nbMiss = sm.getNbMiss();

        runOnUiThread(() -> {
            Intent intent = new Intent(this, GameOverActivity.class);
            intent.putExtra("NBHITS",nbHits);
            intent.putExtra("NBMISS",nbMiss);

            startActivity(intent);

            finish();
        });

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        mSurfaceHeight=height;
        mSurfaceWidth=width;
        Log.d("SimuView", "surfaceChanged " + width + " " + height);
        gameThread.setSurfaceSize(width, height);
        aimThread.setSurfaceSize(width,height);
        loadThread.setSurfaceSize(width,height);

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d("SimuView", "surfaceCreated");
        mPhase=PHASE_LOADING;
        loadThread.setRunning(true);
        loadThread.start();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d("SimuView", "surfaceDestroyed");
        boolean retry = true;
        gameThread.removeListener(this);
        aimThread.removeListener(this);
        loadThread.removeListener(this);
        gameThread.setRunning(false);
        aimThread.setRunning(false);
        loadThread.setRunning(false);
        while (retry) {
            try {
                gameThread.join();
                aimThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }

    }

    @Override
    public void loadThreadFinished() {
        loadThread.removeListener(this);
        mNbProjectilesObtenus = lm.getNbProjectiles();
        mPhase = PHASE_AIMING;
        aimThread.setRunning(true);
        aimThread.start();
    }

    @Override
    public void aimingThreadFinished() {
        aimThread.removeListener(this);
        int prec = am.getPrecision();
        int disp = am.getDispersion();
        sm.init((float)prec,(float)disp,mNbProjectilesObtenus);
        mPhase = PHASE_LAUNCHING;
        gameThread.setRunning(true);
        gameThread.start();

    }

    @Override
    public void onClick(View view) {
        if (mPhase == PHASE_LOADING) lm.trigger();
        if (mPhase == PHASE_AIMING) am.trigger();
    }

}