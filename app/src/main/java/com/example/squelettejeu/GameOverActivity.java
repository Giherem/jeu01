package com.example.squelettejeu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class GameOverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        int nbHits = getIntent().getIntExtra("NBHITS",-1);
        int nbMiss = getIntent().getIntExtra("NBMISS",-1);

        TextView tvGameOver = findViewById(R.id.textviewGameOver);
        tvGameOver.setText("Game Over ! \n"+nbHits+" hits - "+nbMiss+" miss");
    }

    public void goToNext(View view) {
        finish();
    }
}